import { NestFactory } from '@nestjs/core'
import { NestExpressApplication } from '@nestjs/platform-express'

import { AppModule } from './app.module'
import { getEnv } from './config/env'

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule)
  app.enableCors()
  const port = getEnv().port
  await app.listen(port, '0.0.0.0', () => {
    console.clear()
    console.log(`Process PID: ${process.pid} is running on: ${port}`)
    console.log(`Open: http://zeta06.primusweb.com.br:${port}`)
  })

}
bootstrap()
