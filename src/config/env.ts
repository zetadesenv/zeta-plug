export const getEnv = () => {
  return {
    isDevServer: Boolean(process.env.ISDEVSERVER),
    port: Number(process.env.PORT) || 3073,
    prefixo: process.env.CLARION_PREFIXO,
    apiMiddleware: process.env.CLARION_APIMIDDLEWARE,
    authMiddleware: process.env.CLARION_AUTHMIDDLEWARE,
    apiMidia: process.env.CLARION_APIMIDIA,

    URI: process.env.PLUGGTO_URI,
    clienteId: process.env.PLUGGTO_CLENTEID,
    clienteSecret: process.env.PLUGGTO_CLIENTSECRET,
    username: process.env.PLUGGTO_USERNAME,
    password: process.env.PLUGGTO_PASSWORD,
    req: {
      options: {
        headers: {
          Authorization: process.env.CLARION_AUTHMIDDLEWARE,
          accept: 'application/json'
        }
      }
    }
  }
}

