import { Injectable } from '@nestjs/common';

import { getEnv } from './config/env'

@Injectable()
export class AppService {
  getStatus(): Object {
    return {
      server:{
        status: 'ok',
        pid: process.pid,
      },
    };
  }
}
