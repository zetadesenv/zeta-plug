import { Controller, Post, Request, Response } from '@nestjs/common';

import { getEnv } from '../config/env';
import { ProdutoService } from './produto.service';

@Controller('/zwplug/produtos')
export class ProdutoController {
    constructor(
        private readonly produtoService: ProdutoService,
    ) { }

    @Post('/:sku')
    private async inserir(@Request() req, @Response() res) {
        const token = req.headers['authorization'];
        if (token && token == getEnv().authMiddleware) {
            await this.produtoService.buscarESalvarProduto(req.params.sku);
            res.sendStatus(200);
        }
        else
            res.sendStatus(401);
    }
}
