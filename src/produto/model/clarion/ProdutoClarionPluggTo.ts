
export class ProdutoClarion {
    sku: string
    ean: string
    name: string
    external: string
    quantity: number
    special_price: number
    price: number
    short_description: string
    description: string
    brand: string //só nome
    cost: number
    warranty_time: number
    warranty_message: string
    link: string
    available: number //0 ou 1
    categories: any[] //CategoriesPluggtoDTO[]
    handling_time: number
    manufacture_time: number
    dimension: any //DimensionPluggtoDTO
    real_dimension: any //DimensionPluggtoDTO
    attributes: any[] //AttributesPluggtoDTO[]
    photos: Photos[]
    model: any
    aplicacao: string
    flag2: boolean
}

export class Photos {
    url: string //"http://www.site.com.br/enderecoimagem.jpg"
    name: string //"imagem 1 do produto"
    title: string //"imagem 1 do produto"
    order: number
    external: string //"http://www.site.com.br/enderecoimagem.jpg"
}
