import { Injectable } from '@nestjs/common';
import axios from 'axios';

import { ProdutoPluggto } from 'infraestructure/models/pluggto/ProdutoPluggto';

import { getEnv } from '../config/env';
import { ProdutoClarionGateway } from '../infraestructure/clarion/ProdutoClarionGateway';
import { ProdutoClarionPluggtoMapper } from '../infraestructure/maper/ProdutoClarionPluggtoMapper';

@Injectable()
export class ProdutoService {

    private infoToken

    public async buscarESalvarProduto(sku) {
        const gateway = new ProdutoClarionGateway();
        const mapper = new ProdutoClarionPluggtoMapper();

        const produtoClarion: any = await gateway.buscarProduto(sku);
        if (produtoClarion.flag2) {
            const produtoPluggto: ProdutoPluggto = mapper.toTarget(produtoClarion);
            await this.salvarProduto(sku, produtoPluggto);
        }
    }

    public async salvarProduto(sku: string, data: ProdutoPluggto) {
        if (!this.infoToken || this.tokenExpirado()) {
            const token = await this.geraNovoToken();
            const timestampExpirar = new Date().getTime() + token.expires_in * 1000;
            this.infoToken = {
                token: token.access_token,
                tempoExpirar: timestampExpirar
            };
        }

        await this.enviarProduto(sku, data);
    }

    async enviarProduto(sku: string, data: ProdutoPluggto) {
        const options = {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
        };

        try {
            const produtoEnviado = await axios.put(
                getEnv().URI + "/skus/" + sku + "?access_token=" + this.infoToken.token,
                data,
                options
            );
            console.log("\n\nSucesso ao enviar produto")
        } catch (error) {
            console.dir(error?.response?.data)
            console.log("\n\nErro ao enviar produto")
        }

    }

    tokenExpirado() {
        const timestampAtual = new Date().getTime();
        const timestampExpirar = this.infoToken.tempoExpirar;
        //Retorna se o horário atual já passou do tempo de token  
        //Mais 5 minutos pra garantir que busque um token antes do prazo previsto
        return timestampAtual > (timestampExpirar + 300000);
    }

    private async geraNovoToken() {
        const options = {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        };

        //Novo Token
        const response = await axios.post(getEnv().URI + "/oauth/token",
            "grant_type=password&client_id=" + getEnv().clienteId +
            "&client_secret=" + getEnv().clienteSecret +
            "&username=" + getEnv().username +
            "&password=" + getEnv().password,
            options);
        return response.data;
    }
}
