import axios from "axios"

import { getEnv } from "../../config/env"
import { ProdutoClarion } from "../../produto/model/clarion/ProdutoClarionPluggTo"

export class ProdutoClarionGateway {

    public async buscarProduto(cod: string): Promise<ProdutoClarion> {
        const apiProd = getEnv().apiMiddleware + "/" + getEnv().prefixo
        const api = {
            ppro: `${apiProd}ppro/${cod}`,
            zmxo: `${apiProd}zmxo`,
        }
        const response = await axios.get(api.ppro, getEnv().req.options)
        const produto = response.data

        if (!produto) return
        return Object.assign(new ProdutoClarion(), {
            codproduto: produto.codproduto,
            descricao1: produto.descricao1,
            codbarras: produto.codbarras,
            codauxiliar1: produto.codauxiliar1,
            saldoestoque: produto.saldoestoque,
            precovenda1: produto.precovenda1,
            descricao2: produto.descricao2,
            descricao3: produto,
            observacoes: produto.observacoes,
            marca: await this.buscarMarca(produto.codmarca),
            precocusto1: produto.precocusto1,
            garantia_spin_dias: produto.garantia_spin_dias,
            garantia_drop_tempo: produto.garantia_drop_tempo,
            categorias: await this.listarCategorias(produto.codproduto),
            largura: produto.largura,
            profundidade: produto.profundidade,
            altura: produto.altura,
            pesoliquido: produto.pesoliquido,
            fotos: await this.listPhotos(api.zmxo, produto.codproduto, produto.descricao1),
            modelo: produto.codauxiliar1,
            aplicacao: produto.aplicacao,
            flag2: produto.flag2
        })
    }

    public async listPhotos(api: string, cod: string, name: string) {
        const filters = `filter[codigoorigem]=${cod}&filter[tipoorigem]=zw14ppro`
        const fields = 'fields=idmidia, principal, titulo'
        const sort = 'sort=-principal'
        api += `?${filters}&${fields}&${sort}`
        const response = await axios.get(api, getEnv().req.options)

        const photos = []
        response.data.forEach((midia: any, index: number) => {
            const url = `${getEnv().apiMidia}/${midia.idmidia}`
            photos.push({
                url: url,
                name: name,
                title: midia.titulo,
                order: index,
                external: url
            })
        })

        return photos
    }

    public async buscarMarca(cod: string): Promise<any> {
        if (!cod || cod == '0') return undefined
        const apiMarca = getEnv().apiMiddleware + "/" + getEnv().prefixo + "pmar" + "/" + cod
        const response = await axios.get(apiMarca, getEnv().req.options)
        const marca = response.data
        if (!marca || marca === 0) return undefined
        return {
            codmarca: marca.codmarca,
            marca: marca.marca != null ? marca.marca : ""
        }
    }

    public async listarCategorias(codigoProduto) {
        const query = "?q=SELECT zw14pcat.codigo, zw14pcat.descricao, zw14pcat.nivel " +
            "FROM zw14peca, zw14pcat " +
            "WHERE zw14peca.tipoentidade='P' " +
            "AND zw14peca.codentidade='" + codigoProduto + "' " +
            "AND zw14peca.codcategoria=zw14pcat.codigo"
        const apiCategoria = getEnv().apiMiddleware + "/search" + query
        const response = await axios.get(apiCategoria, getEnv().req.options)
        const categoriasBusca = response.data.resultset.rows
        const categorias = categoriasBusca.map((data: any) => {
            return {
                "codigo": data[0],
                "descricao": data[1],
                "nivel": data[2]
            }
        })

        return categorias

    }
}