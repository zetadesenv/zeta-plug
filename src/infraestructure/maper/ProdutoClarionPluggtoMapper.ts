import { ProdutoClarion } from "../models/clarion/ProdutoClarion";
import { ProdutoPluggto } from "../models/pluggto/ProdutoPluggto";
import { CategoriasArrayClarionPluggtoMapper } from "./CategoriasArrayClarionPluggtoMapper";
import { IMapper } from "./IMapper";

export class ProdutoClarionPluggtoMapper implements IMapper<ProdutoClarion, ProdutoPluggto>{

    toTarget(source: ProdutoClarion): ProdutoPluggto {
        this.validaDadosClarion(source);

        const quebrarLinha = (quebraLinha: string) => {
            if (!quebraLinha) return null;
            quebraLinha = quebraLinha.replace(/\r/g, '').trim()
            return quebraLinha.replace(/\n/g, ' <br/> ').trim()
        }

        const dimensao = {
            length: source.profundidade ? source.profundidade : 0,
            width: source.largura ? source.largura : 0,
            height: source.altura ? source.altura : 0,
            weight: source.pesoliquido ? source.pesoliquido : 0
        };

        const atributos = [];

        const categorias = new CategoriasArrayClarionPluggtoMapper().toTarget(source.categorias);

        const gerarObservacao = (): string => {
            //const observacoes = quebrarLinha(source?.observacoes)
            //const descricao2 = quebrarLinha(source?.descricao2)
            //return (`${observacoes}<br/><br/>${descricao2}`).trim() || ''

            const observacoes = source.observacoes ? source.observacoes+ '<br/><br/>' : ''
            const aplicacao = source.aplicacao ? '<b>Aplicação</b><br>'+source.aplicacao : ''
            return (`${observacoes}${aplicacao}`).trim() || ''
        }

        const plug = Object.assign(new ProdutoPluggto(), {
            sku: source.codproduto,
            ean: source.codbarras ? source.codbarras : '',
            name: source.descricao2,
            external: source.codauxiliar1 ? source.codauxiliar1 : '',
            quantity: source.saldoestoque ? source.saldoestoque : 0,
            special_price: source.precovenda1 ? source.precovenda1 : 0,
            price: source.precovenda1 ? source.precovenda1 : 0,
            short_description: ' ',
            description: gerarObservacao(),
            brand: (source.marca && source.marca.marca) ? source.marca.marca : '',
            cost: source.precocusto1 ? source.precocusto1 : 0,
            warranty_time: source.garantia_spin_dias ? source.garantia_spin_dias : 0,
            warranty_message: (source.garantia_spin_dias && source.garantia_drop_tempo) ? source.garantia_spin_dias + " " + source.garantia_drop_tempo : '',
            link: '',
            available: (source.saldoestoque > 1) ? 1 : 0,
            categories: categorias,
            handling_time: 0,
            manufacture_time: 0,
            dimension: dimensao,
            real_dimension: dimensao,
            attributes: atributos,
            photos: source.fotos,
            model: source.modelo,
        })
        return plug

    }

    //Valida os campos do Clarion que são obrigatórios no Pluggto
    private validaDadosClarion(produtoClarion: ProdutoClarion) {
        let erros = "";
        if (!produtoClarion)
            erros = "Produto não foi instanciado!";
        else {
            if (!produtoClarion.codproduto) erros += "codigo do produto vazio;";
            if (!produtoClarion.descricao1) erros += "nome do produto vazio;";
            if (produtoClarion.descricao1.length < 5) erros += "nome do produto deve ter no mínimo 5 caracteres;";
            if (produtoClarion.descricao1.length > 150) erros += "nome do produto deve ter no máximo 150 caracteres;";

            //if (!produtoClarion.saldoestoque) erros += "saldo de estoque vazio;";
            //if (!produtoClarion.precovenda1) erros += "preço de venda vazio;";
        }
        console.log(erros);
        if (erros) throw new Error("Produto Clarion Inválido");
    }

}
