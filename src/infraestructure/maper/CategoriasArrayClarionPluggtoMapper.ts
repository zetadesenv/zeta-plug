import { CategoriaClarion } from "../models/clarion/CategoriaClarion";
import { CategoriesPluggto } from "../models/pluggto/CategoriesPluggto";
import { IMapper } from "./IMapper";

export class CategoriasArrayClarionPluggtoMapper implements IMapper<CategoriaClarion[], CategoriesPluggto[]>{

    toTarget(source: CategoriaClarion[]): CategoriesPluggto[] {
        const target: CategoriesPluggto[] = [];

        const categoriasBase = source.filter((element) => element.nivel == 1);
        const subcategorias = source.filter((element) => element.nivel == 2);
        const tipos = source.filter((element) => element.nivel == 3);

        categoriasBase.forEach(categoriaBase => {
            const valorCategoriaBase = categoriaBase.descricao;
            target.push(Object.assign(
                new CategoriesPluggto(), { "name": valorCategoriaBase }
            ));

            subcategorias.forEach(subcategoria => {
                const valorSubCategoria = valorCategoriaBase.concat(" > ", subcategoria.descricao);
                target.push(Object.assign(
                    new CategoriesPluggto(), { "name": valorSubCategoria }
                ));

                tipos.forEach(tipo => {
                    const valorTipo = valorSubCategoria.concat(" > ", tipo.descricao);
                    target.push(Object.assign(
                        new CategoriesPluggto(), { "name": valorTipo }
                    ));
                });
            });
        });
        return (target);
    }

    toSource(target: CategoriesPluggto[]): CategoriaClarion[] {
        throw new Error("Method not implemented.");
    }

}
