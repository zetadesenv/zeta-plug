import { Photos } from "./PhotoPluggto";

export class ProdutoPluggto {
    sku: string;
    ean: string;
    name: string;
    external: string;
    quantity: number
    special_price: number;
    price: number;
    short_description: string;
    description: string;
    brand: string; //só nome
    cost: number;
    warranty_time: number;
    warranty_message: string;
    link: string;
    available: number; //0 ou 1
    categories: any[]; //CategoriesPluggto[];
    handling_time: number;
    manufacture_time: number;
    dimension: any; //DimensionPluggto;
    real_dimension: any; //DimensionPluggto;
    attributes: any[]; //AttributesPluggto[];
    photos: Photos[];

}