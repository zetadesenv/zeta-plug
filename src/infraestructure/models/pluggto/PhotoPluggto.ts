export class Photos {
    url: string; //"http://www.site.com.br/enderecoimagem.jpg"
    name: string; //"imagem 1 do produto"
    title: string; //"imagem 1 do produto"
    order: number;
    external: string; //"http://www.site.com.br/enderecoimagem.jpg"
}