export class ProdutoClarion {
    codproduto: string;
    descricao1: string;
    codbarras: string;
    // descricao2: string;
    codauxiliar1: string;
    //codauxiliar2: string;
    saldoestoque: number;
    precovenda1: number;
    descricao2: string;
    descricao3: string;
    observacoes: string;
    marca: any; // MarcaClarion;
    precocusto1: number;
    garantia_spin_dias: number;
    garantia_drop_tempo: string;
    // categorias - a ver
    categorias: any[]; // CategoriaClarion[];
    largura: number;
    profundidade: number;
    altura: number;
    pesoliquido: number;
    fotos: any[];
    modelo: any;
    aplicacao: string;
}
