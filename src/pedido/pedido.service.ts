import { Injectable } from '@nestjs/common';
import axios from 'axios';

import { getEnv } from '../config/env';
import { ProdutoClarionPluggtoMapper } from '../infraestructure/maper/ProdutoClarionPluggtoMapper';
import { ProdutoClarionGateway } from '../infraestructure/ProdutoClarionGateway';

@Injectable()
export class PedidoService {

    private infoToken
    
    public async buscarESalvarProduto(sku) {
        const gateway = new ProdutoClarionGateway();
        const mapper = new ProdutoClarionPluggtoMapper();

        const produtoClarion = await gateway.buscarProduto(sku);
        const produtoPluggto = mapper.toTarget(produtoClarion as any);
        await this.salvarProduto(sku, produtoPluggto);
    }

    public async salvarProduto(sku, data) {
        if (!this.infoToken || this.tokenExpirado()) {
            const token = await this.geraNovoToken();
            const timestampExpirar = new Date().getTime() + token.expires_in * 1000;
            this.infoToken = {
                token: token.access_token,
                tempoExpirar: timestampExpirar
            };
        }

        await this.enviarProduto(sku, data);
    }

    async enviarProduto(sku, data) {
        const options = {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        };

        console.log("URI para enviar produto", getEnv().URI + "/skus/" + sku + "?access_token=" + this.infoToken.token);
        await axios.put(
            getEnv().URI + "/skus/" + sku + "?access_token=" + this.infoToken.token,
            data,
            options
        );

    }

    tokenExpirado() {
        const timestampAtual = new Date().getTime();
        const timestampExpirar = this.infoToken.tempoExpirar;
        //Retorna se o horário atual já passou do tempo de token  
        //Mais 5 minutos pra garantir que busque um token antes do prazo previsto
        return timestampAtual > (timestampExpirar + 300000);
    }

    private async geraNovoToken() {
        const options = {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        };

        //Novo Token
        const response = await axios.post(getEnv().URI + "/oauth/token",
            "grant_type=password&client_id=" + getEnv().clienteId +
            "&client_secret=" + getEnv().clienteSecret +
            "&username=" + getEnv().username +
            "&password=" + getEnv().password,
            options);
        return response.data;
    }
}
