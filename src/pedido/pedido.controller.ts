import { Controller, Post, Request, Response } from '@nestjs/common';

import { getEnv } from '../config/env';
import { PedidoService } from './pedido.service';

@Controller('/zwplug/produtos')
export class PedidoController {
    constructor(
        private readonly pedidoService: PedidoService,
    ) { }

    @Post('/:sku')
    private async inserir(@Request() req, @Response() res) {
        const token = req.headers['authorization'];
        if (token && token == getEnv().authMiddleware) {
            await this.pedidoService.buscarESalvarProduto(req.params.sku);
            res.sendStatus(200);
        }
        else
            res.sendStatus(401);
    }
}
