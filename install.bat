@echo off
echo.
echo.
echo             _                          _                     _
echo            ^| ^|                        ^| ^|                   ^| ^|
echo     _______^| ^|_ __ _   _ __   ___   __^| ^| ___  __      _____^| ^|__
echo    ^|_  / _ \ __/ _^` ^| ^| ^'_ \ / _ \ / _^` ^|/ _ \ \ \ /\ / / _ \ ^'_ \
echo     / /  __/ ^|^| (_^| ^| ^| ^| ^| ^| (_) ^| (_^| ^|  __/  \ V  V /  __/ ^|_) ^|
echo    /___\___^|\__\__,_^| ^|_^| ^|_^|\___/ \__,_^|\___^|   \_/\_/ \___^|_.__/
echo.
echo    v0.1.0
echo.
set base_dir="%cd%"
net session >nul 2>&1
if not %errorLevel% == 0 (
    echo.
    echo.
    echo ERRO: O usuario atual nao possui permissoes de administrador
    goto :exit
)
where /q node.exe
if %errorlevel% == 1 (
  echo.
  echo.
  echo ERRO: Nao foi possivel encontrar node.exe,
  echo       verifique se foi instalado corretamente e se esta disponivel no PATH.
  goto :exit
)
for /f "delims=" %%i in ('where node.exe') do set node=%%i
for /f "tokens=1" %%i in ('node.exe -v') do set node_version=%%i
if not exist bin\srvany.exe (
  echo.
  echo.
  echo ERRO: O utilitario srvany.exe nao foi encontrado em %base_dir%\bin.
  goto :exit
)
if not defined ZETA_HOME (
  echo.
  echo.
  echo WARN: A variavel de ambiente ZETA_HOME nao foi configurada,
  echo       ignore se estiver utilizando configuracoes locais ^(.env^).
)
:configuration
set name=zeta_plug
set port=3002
set bind=0.0.0.0
echo.
echo.
choice /m "1. Deseja customizar o nome do servico [%name%]? [s/n]" /c sn /n
if %errorlevel% == 1 (
  :custom_name
  echo.
  set /p name="Que nome voce gostaria de usar? "
)
sc query %name% | findstr "1060" > nul 2>&1
if not %errorlevel% == 0 (
  echo.
  echo.
  echo INFO: Ja existe um servico com o nome %name%, escolha outro nome.
  echo.
  goto :custom_name
)
echo.
choice /m "2. Deseja vincular o servico a uma interface de rede especifica [s/n]?" /c sn /n
if %errorlevel% == 1 (
  echo.
  set /p bind="Qual o hostname ou endereco IP da interface a qual voce deseja vincular o servico? "
)
echo.
choice /m "3. Deseja customizar a porta [%port%] na qual o servico sera executado? [s/n]" /c sn /n
if %errorlevel% == 1 (
  :custom_port
  echo.
  set /p port="Que porta voce gostaria de usar? "
)
netstat -abnovp tcp | findstr /r "^TCP %bind%:%port%" > nul 2>&1
if %errorlevel% == 0 (
  echo.
  echo.
  echo INFO: Ja existe um servico executando na porta %port%.
  echo.
  goto :custom_port
)
echo.
echo.
echo INFO: O nome do servico sera %name%.
echo.
echo INFO: A execucao do servico sera vinculada a %bind%:%port%.
echo.
echo INFO: O servico sera executado em:
echo.
echo       %base_dir%
echo.
echo INFO: Utilizando a versao %node_version% do NodeJS instalada em:
echo.
echo       "%node%"
echo.
echo.
choice /m "4. Isto esta correto? [s/n]" /c sn /n
if %errorlevel% == 2 (
  goto :configuration
)
echo.
echo.
echo INFO: Aguarde enquanto as dependencias sao instaladas. . .
echo.
call npm i --production
if not %errorlevel% == 0 (
  echo.
  echo.
  echo ERRO: Houve uma falha durante a instalacao/atualizacao de dependencias
  goto :exit
)
echo.
echo.
echo INFO: Aguarde enquando criamos o novo servico. . .
sc create %name% binPath= "%base_dir%\bin\srvany.exe" DisplayName= "%name%" start= auto > nul 2>&1
REG ADD HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\%name%\Parameters /v AppDirectory /t REG_SZ /d "%base_dir%" >nul 2>&1
REG ADD HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\%name%\Parameters /v Application /t REG_SZ /d "%node%" >nul 2>&1
REG ADD HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\%name%\Parameters /v AppParameters /t REG_SZ /d "%base_dir%\dist\main.js %port%" >nul 2>&1
echo.
echo.
echo INFO: Iniciando o servico. . .
sc start %name% > nul 2>&1
if %errorlevel% == 1 (
  echo.
  echo.
  echo ERRO: Houve um erro ao iniciar o servico.
  goto :exit
)
echo.
echo.
echo INFO: O servico foi criado e iniciado com sucesso.
echo.
set description="App is running in port %port%"
sc description %name% %description%
sc query %name%
echo.
echo %description%
:exit
echo.
echo.
pause
